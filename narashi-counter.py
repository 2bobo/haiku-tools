# coding: UTF-8
import json
import csv
import re
import urllib
import requests

import pandas as pd

HAIKU_URL = "http://h.hatena.ne.jp"
TIMELINE_PATH = "/api/statuses/keyword_timeline.json"

def create_url(query):
    query_keyword = urllib.parse.urlencode(query)

    get_timeline_url = urllib.parse.urljoin(HAIKU_URL, TIMELINE_PATH)

    return get_timeline_url + "?" + query_keyword


def get_timeline(keyword):
    cnt = 1
    timeline_list = []
    while True:
        ayataka = {"word": keyword, "count": "200", "page":cnt}
        get_url = create_url(ayataka)
        response = requests.get(get_url)
        if response.text != "[]":
            timeline_list.extend(json.loads(response.text))
            cnt += 1
        else:
            break
    return timeline_list


def post_parser(timeline_list):

    parse_posts = []

    for post in (reversed(timeline_list)):
        parse_post = {
            "source": post["source"],
            "url":post["link"],
            "date":post["created_at"],
            "text":post["text"],
            "user":post["user"]["name"],
            "id": post["user"]["id"]
        }
        parse_posts.append(parse_post)
    return parse_posts


def write_csv(data):
    with open('data2.csv', 'w') as file:
        writer = csv.writer(file, lineterminator='\n')
        writer.writerows(data)


def read_csv(csv_file_name):
    with open(csv_file_name, newline='') as csvfile:
        spamreader = csv.reader(csvfile)
        data = [e for e in spamreader]

    return data


def ayataka(posts):
    ayataka_nokori_pattern = r"残り[0-9]{1,4}(本|綾鷹)"
    ayataka_nokori = re.compile(ayataka_nokori_pattern)
    ayataka_honsuu_pattern = r"[0-9]{1,4}"
    ayataka_honsuu = re.compile(ayataka_honsuu_pattern)

    ayataka_data = [["投稿日", "ユーザー名", "ユーザーID", "URL", "投稿方法", "本文", "残り本数"]]

    for post in posts:
        nokori = ayataka_nokori.search(post["text"])
        if nokori:
            hon = ayataka_honsuu.findall(nokori.group(0))
            if len(hon) == 1:
                post["nokori"] = str(hon[0])
            else:
                post["nokori"] = None
        else:
            post["nokori"] = None

        ayataka_data.append([post["date"], post["user"], post["id"], post["url"], post["source"], post["text"], post["nokori"]])

    return ayataka_data


if __name__ == '__main__':
    # データ取得
    keyword = "綾鷹千本選ばれ慣らし"
    timeline_list = post_parser(get_timeline(keyword))

    # test
    #timeline_list = [{'source': 'koHaiku', 'url': 'http://h.hatena.ne.jp/Akkiesoft/171941783743324878', 'date': '2013-01-10T11:17:44Z', 'text': '綾鷹千本選ばれ慣らし=夜も選ばれる。今日からカウントするので残り999本\nhttp://cdn-ak.f.st-hatena.com/images/fotolife/A/Akkiesoft/20130110/20130110201736.jpg', 'user': 'あっきぃ@2日東ナ11b', 'id': 'Akkiesoft'}]
    timeline_list = ayataka(timeline_list)

    write_csv(timeline_list)

    print(keyword)
